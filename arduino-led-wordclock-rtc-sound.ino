#include <Adafruit_NeoPixel.h>
#include <avr/power.h>
#include <TimeLib.h>
#include <Wire.h>
#include "RTClib.h"

#define SOUNDSENSOR   12
#define PIN           4
#define NUMPIXELS     165

Adafruit_NeoPixel pixels = Adafruit_NeoPixel(NUMPIXELS, PIN, NEO_GRB + NEO_KHZ800);

const unsigned long lightUpSeconds = 600 * 1000UL; // 10 minutes
int h; int m; int s;
RTC_DS3231 rtc;
boolean showWatch = false;
boolean currentShowWatch = false;

void setup() {
  pinMode(SOUNDSENSOR, INPUT);
  pinMode(PIN, OUTPUT);
  if (! rtc.begin()) {
    Serial.println("Couldn't find RTC");
    while (1);
  }
  rtc.adjust(DateTime(2018, 11, 21, 11, 50, 0)); // y, m, d, h, m, s
  //if (rtc.lostPower()) {} - #TODO: ENLIGHT SOME SPECIAL LETTERS AND DELAY
  pixels.begin();
}

void loop() {
  /* Get current time for variables h m s */
  DateTime now = rtc.now();
  h = now.hour(); 
  m = now.minute();
  s = now.second();
  if (h>12) { h-=12; }

  static unsigned long lastSampleTime = 0 - lightUpSeconds;  
  if(digitalRead(SOUNDSENSOR) == 1) { showWatch = true; }
  if (millis() - lastSampleTime >= lightUpSeconds) {
    lastSampleTime += lightUpSeconds;
    showWatch = false;
  }

  if((s == 0 && (m % 5) == 0) || (showWatch != currentShowWatch)) {
    currentShowWatch = showWatch;
    int ledPixels[NUMPIXELS] = {
    0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
    0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
    0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0 };

    if(showWatch) {
    /* ES IST */
    ledPixels[151]=1;ledPixels[152]=1;ledPixels[153]=1;ledPixels[154]=1;ledPixels[155]=1;ledPixels[156]=1; 

    /* FÜNF vor/nach */ if ((m >= 5 && m < 10) || (m >= 55 && m < 60) || (m >= 25 && m < 30) || (m >= 35 && m < 40)) {
      ledPixels[157] = 1; ledPixels[158] = 1; ledPixels[159] = 1; ledPixels[160] = 1; ledPixels[161] = 1; ledPixels[162] = 1; ledPixels[163] = 1; ledPixels[164] = 1; ledPixels[165] = 1;
    }
    /* ZEHN vor/nach */ else if ((m >= 10 && m < 15) || (m >= 50 && m < 55)) {
      ledPixels[145] = 1; ledPixels[146] = 1; ledPixels[147] = 1; ledPixels[148] = 1; ledPixels[149] = 1; ledPixels[150] = 1;
    }
    /* ZWANZIG */ else if ((m >= 20 && m < 25) || (m >= 40 && m < 45)) {
      ledPixels[134] = 1; ledPixels[135] = 1; ledPixels[136] = 1; ledPixels[137] = 1; ledPixels[138] = 1;
      ledPixels[139] = 1; ledPixels[140] = 1; ledPixels[141] = 1; ledPixels[142] = 1; ledPixels[143] = 1; ledPixels[144] = 1;
    }

    /* VIERTEL [!vor/]nach */ if ((m >= 15 && m < 20)) {
      ledPixels[118] = 1; ledPixels[119] = 1; ledPixels[120] = 1; ledPixels[121] = 1; ledPixels[122] = 1;ledPixels[123] = 1; ledPixels[124] = 1; ledPixels[125] = 1; ledPixels[126] = 1; ledPixels[127] = 1;
    }
    /* DREIVIERTEL */ else if ((m >= 45 && m < 50)) {
      ledPixels[89] = 1; ledPixels[90] = 1; ledPixels[91] = 1; ledPixels[92] = 1; ledPixels[93] = 1;ledPixels[94] = 1; ledPixels[95] = 1; ledPixels[96] = 1; ledPixels[97] = 1; ledPixels[98] = 1; ledPixels[99] = 1;
      ledPixels[100] = 1; ledPixels[101] = 1; ledPixels[102] = 1; ledPixels[103] = 1; ledPixels[104] = 1; ledPixels[105] = 1;
      h++;
    }
    /* HALB */ else if ((m >= 25 && m < 40)) {
      ledPixels[106] = 1; ledPixels[107] = 1; ledPixels[108] = 1; ledPixels[109] = 1; ledPixels[110] = 1; ledPixels[111] = 1; ledPixels[112] = 1; ledPixels[113] = 1;
      h++; // refer to next hour
    }

    /* NACH */ if (((m >= 5) && (m < 25)) || ((m >= 35) && (m < 40))) {
      ledPixels[128] = 1; ledPixels[129] = 1; ledPixels[130] = 1; ledPixels[131] = 1; ledPixels[132] = 1; ledPixels[133] = 1;
    }

    /* VOR */ else if ((m >= 40 && m < 45) || (m >= 50) || (m >= 25 && m < 30)) {
      ledPixels[114] = 1; ledPixels[115] = 1; ledPixels[116] = 1; ledPixels[117] = 1;
      if (m >= 40) {
        h++; // refer to next hour if not already referred by HOIBE
      }
    }

    /* Hours */
    if (h == 0) { h = 12; } 
    if (h == 1) { ledPixels[83] = 1;ledPixels[84] = 1;ledPixels[85] = 1;ledPixels[86] = 1;ledPixels[87] = 1;ledPixels[88] = 1;}
      else if (h == 2) { ledPixels[61] = 1;ledPixels[62] = 1;ledPixels[63] = 1;ledPixels[64] = 1;ledPixels[65] = 1;ledPixels[66] = 1;}
      else if (h == 3) { ledPixels[55] = 1;ledPixels[56] = 1;ledPixels[57] = 1;ledPixels[58] = 1;ledPixels[59] = 1;ledPixels[60] = 1;}
      else if (h == 4) { ledPixels[67] = 1;ledPixels[68] = 1;ledPixels[69] = 1;ledPixels[70] = 1;ledPixels[71] = 1;ledPixels[72] = 1;ledPixels[73] = 1;ledPixels[74] = 1;}
      else if (h == 5) { ledPixels[75] = 1;ledPixels[76] = 1;ledPixels[77] = 1;ledPixels[78] = 1;ledPixels[79] = 1;ledPixels[80] = 1;ledPixels[81] = 1;ledPixels[82] = 1;}
      else if (h == 6) { ledPixels[46] = 1;ledPixels[47] = 1;ledPixels[48] = 1;ledPixels[49] = 1;ledPixels[50] = 1;ledPixels[51] = 1;ledPixels[52] = 1;ledPixels[53] = 1;ledPixels[54] = 1;  }
      else if (h == 7) { ledPixels[30] = 1;ledPixels[31] = 1;ledPixels[32] = 1;ledPixels[33] = 1;ledPixels[34] = 1;ledPixels[35] = 1;ledPixels[36] = 1;ledPixels[37] = 1;}
      else if (h == 8) { ledPixels[38] = 1;ledPixels[39] = 1;ledPixels[40] = 1;ledPixels[41] = 1;ledPixels[42] = 1;ledPixels[43] = 1;ledPixels[44] = 1;ledPixels[45] = 1;}
      else if (h == 9) { ledPixels[22] = 1;ledPixels[23] = 1;ledPixels[24] = 1;ledPixels[25] = 1;ledPixels[26] = 1;ledPixels[27] = 1;ledPixels[28] = 1;ledPixels[29] = 1;}
      else if (h == 10) { ledPixels[16] = 1;ledPixels[17] = 1;ledPixels[18] = 1;ledPixels[19] = 1;ledPixels[20] = 1;ledPixels[21] = 1;}
      else if (h == 11) { ledPixels[0] = 1; ledPixels[1] = 1;ledPixels[2] = 1;ledPixels[3] = 1;ledPixels[4] = 1;ledPixels[5] = 1;}
      else if (h == 12 || h == 0) { ledPixels[6] = 1;ledPixels[7] = 1;ledPixels[8] = 1;ledPixels[9] = 1;ledPixels[10] = 1;ledPixels[11] = 1;ledPixels[12] = 1;ledPixels[13] = 1;ledPixels[14] = 1;ledPixels[15] = 1;}
      else if (h >= 13) { ledPixels[83] = 1;ledPixels[84] = 1;ledPixels[85] = 1;ledPixels[86] = 1;ledPixels[87] = 1;ledPixels[88] = 1;} //Set time to one
    }
  
    for (int i = 0; i < NUMPIXELS; i++) {
      if (ledPixels[i] == 1) { pixels.setPixelColor(i, pixels.Color(60, 60, 60)); } //Set Neopixel color / brightness
      else { pixels.setPixelColor(i, pixels.Color(0, 0, 0)); }
    }
    pixels.show();
  }
}
